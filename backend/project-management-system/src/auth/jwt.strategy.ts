import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AuthService } from './auth.service';

import { UserDTO } from '../models/user/user.dto';
import { TokenPayload } from '../models/auth/token-payload.model';


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(private readonly authService: AuthService, private readonly config: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      // ^ token is taken from the Authorization request header using the bearer scheme
      ignoreExpiration: false,
      secretOrKey: config.get('JWT_SECRET'),
    });    
  }
// only executes validate method if the token above is valid (1st parameter of validate is the decoded payload from the token)

/* Recall again that Passport will build a user object based ->
  on the return value of our validate() method, 
  and attach it as a property on the Request object.
*/

  async validate(payload: TokenPayload): Promise<UserDTO> {          
    const user = await this.authService.validateUser(payload.email);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}