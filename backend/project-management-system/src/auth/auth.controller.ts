import { CreateUserDTO } from './../models/user/create-user.dto';
import { LoginCredentials } from './../models/auth/login-credentials.dto';
import { AuthService } from './auth.service';
import { Controller, Get, Post, Body,  UseGuards, Delete } from '@nestjs/common';
import { TokenDTO } from '../models/auth/token.dto';

@Controller('auth')
export class AuthController {

constructor(
    private readonly authService: AuthService,
) {}

@Get()
  async testAuth() {
    return 'Success!!';
  }

// LOGIN 

@Post('login')
async login(@Body() credentials: LoginCredentials): Promise<TokenDTO> {
    return this.authService.login(credentials);
}

// REGISTER

// DELETE ONCE THERE IS A SEED SCRIPT!!!!

@Post('register')
async register(@Body() createUser: CreateUserDTO): Promise<Object> {
    return this.authService.register(createUser)
}


}
