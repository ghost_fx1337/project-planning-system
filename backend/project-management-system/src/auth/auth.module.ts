import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './../user/user.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Module, Global } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy'
import { User } from '../data/entities/user.entity';

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    ConfigModule, 
    UserModule, 
    PassportModule.register({ defaultStrategy: 'jwt' }), JwtModule.registerAsync({
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: async (config: ConfigService) => ({
      secret: config.get('JWT_SECRET'),
      signOptions: {
        expiresIn: '30d',
      }
    })
  })],
  controllers: [AuthController],
  exports: [AuthService],
  providers: [AuthService, JwtStrategy,],
})
export class AuthModule { }
