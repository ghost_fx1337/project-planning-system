import { User } from '../../data/entities/user.entity';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const GetUser = createParamDecorator(
  (data, req): User => {
    const request = req.switchToHttp().getRequest();
    return request.user;
  },
);