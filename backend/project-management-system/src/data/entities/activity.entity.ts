import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from "typeorm";
import { Skill } from "./skill.entity";
import { Project } from "./project.entity";
import { Member } from "./member.entity";


@Entity('activities')
export class Activity {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column()
    hours: number;

    @ManyToOne((type) => Skill, (skill) => skill.activities)
    skill: Promise<Skill>;

    @ManyToOne((type) => Project, (project) => project.activities)
    project: Promise<Project>;

    @OneToMany((type) => Member, (member) => member.activity)
    members: Promise<Member[]>;

}