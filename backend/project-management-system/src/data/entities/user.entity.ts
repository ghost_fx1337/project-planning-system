import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, ManyToMany, JoinTable } from "typeorm";
import { UserRoles } from "../../enums/user-role.enum";
import { Skill } from "./skill.entity";
import { Member } from "./member.entity";



@Entity('users')
export class User {

    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column('nvarchar')
    firstName: string;

    @Column('nvarchar')
    surname: string;

    @Column('nvarchar')
    email: string;

    @Column('nvarchar')
    position: string;

    @Column('nvarchar')
    password: string;

    @Column({ type: 'enum', enum: UserRoles, default: UserRoles.basic })
    role: UserRoles;

    @Column({ type: 'longblob', default: null})
    avatar: string;

    @Column({ type: 'boolean', default: false })
    isDeleted: boolean;

    @OneToMany((type) => User, (user) => user.managedBy)
    subordinates: Promise<User[]>;

    @ManyToOne((type) => User, (user) => user.subordinates)
    managedBy: Promise<User>;
    
    @ManyToMany((type) => Skill)
    @JoinTable()
    skills: Promise<Skill[]>;

    @OneToMany((type) => Member, (member) => member.user)
    projectsMember: Promise<Member[]>;

}