import { UserRoles } from './../../enums/user-role.enum';
import { createConnection, Repository } from "typeorm";
import * as bcrypt from 'bcrypt';
import { User } from "../entities/user.entity";


const seedAdmin = async (connection: any) => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);

    const admin = await userRepo.findOne({
        where: {
            name: 'admin',
        },
    });

    if (admin) {
        console.log('The DB already has an admin!');
        return;
    }

    const firstName: string = 'admin';
    const surname: string = 'admin';
    const email: string = 'admin@domain.com';
    const position: string = 'admin';
    const password: string = '!1Password';
    const hashedPassword = await bcrypt.hash(password, 10);

    const newAdmin: User = userRepo.create({
        firstName,
        surname,
        email,
        password: hashedPassword,
        position,
        role: UserRoles.admin,
    });

    await userRepo.save(newAdmin);
    console.log('Seeded admin successfully!');
};

const seed = async () => {
    console.log('Seed started!');
    const connection = await createConnection();

    await seedAdmin(connection);

    await connection.close();
    console.log('Seed completed!');
};

seed().catch(console.error);