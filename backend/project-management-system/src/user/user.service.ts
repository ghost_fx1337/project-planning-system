import { ActivityDTO } from 'src/models/project/activity.dto';
import { ProjectDTO } from 'src/models/project/project.dto';
import { MemberDTO } from './../models/project/member.dto';
import { SkillDTO } from './../models/admin/skill.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../data/entities/user.entity';
import { UserDTO } from '../models/user/user.dto';
import { plainToClass } from 'class-transformer';
import { UserRoles } from '../enums/user-role.enum';
import { Project } from '../data/entities/project.entity';
import { Skill } from '../data/entities/skill.entity';
import { ProjectStatus } from 'src/enums/project-status.enum';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User) private readonly usersRepo: Repository<User>,
    ) { };

    async getUserDetails(id: string): Promise<UserDTO> {
        let user = await this.usersRepo.findOne({
            where: { id }
        })
        return this.convertUser(user);
    }

    async getAllUsers(): Promise<UserDTO[]> {
        let users = await this.usersRepo.find()
        let usersConverted = [];
        for (let user of users) {
            usersConverted.push(await this.convertUser(user));
        }
        return usersConverted;
    }

    async getAllBasicUsers(): Promise<UserDTO[]> {
        let users = await this.usersRepo.find({ where: { role: UserRoles.basic } })
        let usersConverted = [];
        for (let user of users) {
            usersConverted.push(await this.convertUser(user));
        }
        return usersConverted;
    }

    // Convert User to UserDTO, resolve all relations.
    private async convertUser(user: User): Promise<UserDTO> {
        const converted = plainToClass(UserDTO, user, {
            excludeExtraneousValues: true,
        });

        converted.skills = plainToClass(SkillDTO, (await user.skills), {
            excludeExtraneousValues: true,
        })

        converted.managedBy = plainToClass(UserDTO, (await user.managedBy), {
            excludeExtraneousValues: true,
        })

        converted.subordinates = plainToClass(UserDTO, (await user.subordinates), {
            excludeExtraneousValues: true,
        })

        let members = await user.projectsMember;
        converted.projectsMember = [];
        for (let member of members) {
            if (member.endDate) {
                continue;
            }
            let memberDTO = plainToClass(MemberDTO, member, {
                excludeExtraneousValues: true,
            });

            let activity = await member.activity;
            if (activity) {
                let project = await activity.project;


                if (project.status !== ProjectStatus.inProgress) {
                    continue;
                }
                memberDTO.activity = plainToClass(ActivityDTO, activity, {
                    excludeExtraneousValues: true,
                });

                memberDTO.activity.project = plainToClass(ProjectDTO, project, {
                    excludeExtraneousValues: true,
                });
            } 
            converted.projectsMember.push(memberDTO)
        }

        //TODO pupulate projects and skills in projectsMember array
        return converted;
    }

}
