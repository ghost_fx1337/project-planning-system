import { Controller, UseGuards, Get, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { APIResponse } from '../models/user/api-response.dto';
import { UserDTO } from '../models/user/user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {

    constructor (
        private readonly userService: UserService,
    ) {}

    // Get user details by id
    @UseGuards(AuthGuard('jwt'))
    @Get('details/:id')
    async getUserDetails (
        @Param('id') id: string,
    ): Promise<APIResponse<UserDTO>>  {
        return this.serializeResponse(await this.userService.getUserDetails(id))
    }

    // Get all users
    @UseGuards(AuthGuard('jwt'))
    @Get('/all')
    async getAllUsers (
    ): Promise<APIResponse<UserDTO[]>>  {
        return this.serializeResponse(await this.userService.getAllUsers())
    }

    // Get all basic users
    @UseGuards(AuthGuard('jwt'))
    @Get('/basic')
    async getAllBasicUsers (
    ): Promise<APIResponse<UserDTO[]>>  {
        return this.serializeResponse(await this.userService.getAllBasicUsers())
    }
    
    private serializeResponse<T>(data: T): APIResponse<T> {
        return { data };
    }

}
