import { ProjectDTO } from '../../models/project/project.dto';
import { SkillDTO } from './../admin/skill.dto';
import { IsNotEmpty } from "class-validator";
import { Expose } from 'class-transformer';
import { MemberDTO } from './member.dto';

export class ActivityDTO {

    @Expose()
    id: string;

    @Expose()
    @IsNotEmpty()
    hours: number;

    @IsNotEmpty()
    skill: SkillDTO;

    @IsNotEmpty()
    members: MemberDTO[];

    @IsNotEmpty()
    project: ProjectDTO;

}