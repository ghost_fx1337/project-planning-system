export class TokenPayload {
    id: string;
    firstName: string;
    surname: string;
    email: string;
  }