import { SkillDTO } from './../admin/skill.dto';
import { IsNotEmpty, Length, Matches } from "class-validator";
import { UserRoles } from "../../enums/user-role.enum";
import { UserDTO } from './user.dto';

export class CreateUserDTO {

    @IsNotEmpty()
    @Length(2, 20)
    firstName: string;

    @IsNotEmpty()
    surname: string;

    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    @Matches(/^[a-z0-9]+$/i)
    password: string;

    @IsNotEmpty()
    position: string

    @IsNotEmpty()
    role: UserRoles
    
    managedBy: UserDTO
    
    skills: SkillDTO[]




}