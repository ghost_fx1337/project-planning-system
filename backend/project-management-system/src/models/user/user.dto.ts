import { MemberDTO } from './../project/member.dto';
import { SkillDTO } from './../admin/skill.dto';
import { Expose, Transform, plainToClass } from 'class-transformer'
import { UserRoles } from '../../enums/user-role.enum';
import { Skill } from '../../data/entities/skill.entity';
import { User } from 'src/data/entities/user.entity';

export class UserDTO {
    @Expose()
    id: string;

    @Expose()
    firstName: string;

    @Expose()
    surname: string;

    @Expose()
    email: string

    @Expose()
    position: string

    @Expose()
    role: UserRoles

    managedBy: UserDTO

    skills: SkillDTO[]

    subordinates: UserDTO[]

    projectsMember: MemberDTO[];

}