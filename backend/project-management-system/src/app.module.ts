import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ProjectModule } from './project/project.module';
import { DatabaseModule } from './data/database.module';
import { AuthModule } from './auth/auth.module';
import { AdminModule } from './admin/admin.module';
import * as Joi from '@hapi/joi';

@Module({
      imports: [ConfigModule.forRoot({
      validationSchema: Joi.object({
      PORT: Joi.number().default(3000),
      DB_TYPE: Joi.string().required(),
      DB_HOST: Joi.string().required(),
      DB_PORT: Joi.number().required(),
      DB_USERNAME: Joi.string().required(),
      DB_PASSWORD: Joi.string().required(),
      DB_DATABASE_NAME: Joi.string().required(),
      JWT_SECRET: Joi.string().required(),
    }),
  }),
    UserModule,
    ProjectModule,
    DatabaseModule,
    AuthModule,
    AdminModule,
  ], 
  controllers: [AppController], // add controllers here...
  providers: [AppService], // add services here...
})
export class AppModule { }
