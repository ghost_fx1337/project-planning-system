import { Member } from './../data/entities/member.entity';
import { Activity } from './../data/entities/activity.entity';
import { Project } from './../data/entities/project.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { ProjectController } from './project.controller';
import { ProjectService } from './project.service';
import { User } from '../data/entities/user.entity';
import { Skill } from '../data/entities/skill.entity';

@Module({
  controllers: [ProjectController],
  providers: [ProjectService],
  imports: [TypeOrmModule.forFeature([Project, Activity, Member, User, Skill])],
})
export class ProjectModule {}
