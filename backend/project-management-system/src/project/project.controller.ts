import { Controller, UseGuards, Post, Body, Get, Param, Patch } from '@nestjs/common';
import { ProjectService } from './project.service';
import { AuthGuard } from '@nestjs/passport';
import { APIResponse } from '../models/user/api-response.dto';
import { ProjectDTO } from '../models/project/project.dto';

@Controller('projects')
export class ProjectController {
    constructor(
        private readonly projectService: ProjectService,
    ) { }


    @UseGuards(AuthGuard('jwt'))
    @Post("project")
    async createProject(
        @Body() projectDTO: ProjectDTO,
    ): Promise<APIResponse<ProjectDTO>> {
        return this.serializeResponse(await this.projectService.createProject(projectDTO))
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('project/:id')
    async getProject(
        @Param('id') id: string,
    ): Promise<APIResponse<ProjectDTO>> {
        return this.serializeResponse(await this.projectService.getProject(id))
    }

    @UseGuards(AuthGuard('jwt'))
    @Patch('project/:id')
    async updateProject(
        @Param('id') id: string,
        @Body() projectDTO: ProjectDTO,
    ): Promise<APIResponse<ProjectDTO>> {
        return this.serializeResponse(await this.projectService.updateProject(id, projectDTO))
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('/all')
    async getAllProject(
    ): Promise<APIResponse<ProjectDTO[]>> {
        return this.serializeResponse(await this.projectService.getAllProject())
    }

    private serializeResponse<T>(data: T): APIResponse<T> {
        return { data };
    }
}
