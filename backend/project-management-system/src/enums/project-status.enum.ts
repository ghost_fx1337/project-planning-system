export enum ProjectStatus {
    inProgress = 'INPROGRESS',
    completed = 'COMPLETED'
}