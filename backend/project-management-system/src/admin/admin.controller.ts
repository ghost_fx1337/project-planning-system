import { UserDTO } from './../models/user/user.dto';
import { Controller, UseGuards, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../common/decorators/get-user.decorator';
import { User } from '../data/entities/user.entity';
import { APIResponse } from '../models/user/api-response.dto';
import { AdminService } from './admin.service';
import { SkillDTO } from '../models/admin/skill.dto';
import { Skill } from '../data/entities/skill.entity';
import { CreateUserDTO } from '../models/user/create-user.dto';

@Controller('admin')
export class AdminController {

    constructor (
        private readonly adminService: AdminService,
    ) {}

    @UseGuards(AuthGuard('jwt'))
    @Post('/skill')
    async createSkill (
        @Body() skillDTO: SkillDTO,
    ): Promise<APIResponse<Skill>>  {
        return this.serializeResponse(await this.adminService.createSkill(skillDTO))
    }

    //TODO add admin guard
    @UseGuards(AuthGuard('jwt'))
    @Get('/skills')
    async getAllSkills (
    ): Promise<APIResponse<SkillDTO[]>>  {
        return this.serializeResponse(await this.adminService.getAllSkills())
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('/user')
    async createEmployee (
        @Body() userDTO: CreateUserDTO,
    ): Promise<APIResponse<UserDTO>>  {
        return this.serializeResponse(await this.adminService.createUser(userDTO))
    }

    @UseGuards(AuthGuard('jwt'))
    @Patch('/user/:id')
    async updateUser (
        @Param('id') id: string,
        @Body() userDTO: UserDTO,
    ): Promise<APIResponse<UserDTO>>  {
        return this.serializeResponse(await this.adminService.updateUser(id, userDTO))
    }
    

    private serializeResponse<T>(data: T): APIResponse<T> {
        return { data };
    }

}
